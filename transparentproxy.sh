#!/bin/bash -e

red=$'\e[0;91m'
green=$'\e[0;92m'
endc=$'\e[0m'

check_root() {
  # Checks if you are UID 0, if not, exits.
  if [[ "$(id -u)" -ne 0 ]]; then
      printf "\\n${red}%s${endc}\\n" \
             "This program needs to be run as root." 2>&1
      exit 1
  fi
}

ip_check() {
  # Check your IP address by using curl with ipinfo.
  if ! external_ip="$(curl -s -m 10 ipinfo.io/geo)"; then
    printf "${red}%s${endc}\\n" "[ failed ] curl: HTTP request error!"
    printf "%s\\n" "Please check your network settings."
  fi

  # Print output.
  printf "${green}%s${endc}\\n"  "IP Address:"
  printf "$external_ip" | tr -d '"{}' | sed 's/ //g'
}

checktorinstall() {
  # Checks if Tor is installed.
  if ! [ -x "$(command -v tor)" ]; then
    printf "\\n${red}%s${endc}\\n" \
           "Tor is not installed. Installing now."
    # Installs Tor.
    pacman -S --noconfirm -q tor >/dev/null
  else
      printf "\\n${green}%s${endc}\\n" \
             "Tor is installed."
  fi
}

start() {
  # Checks if Tor is installed.
  checktorinstall

  # Stops systemd services.
  if systemctl is-active tor.service >/dev/null 2>&1; then
    systemctl stop tor.service
  fi

  if systemctl is-active iptables.service >/dev/null 2>&1; then
    systemctl stop iptables.service
  fi

  if systemctl is-active ip6tables.service >/dev/null 2>&1; then
    systemctl stop ip6tables.service
  fi

  # Create backups.
  mkdir /root/tor-backups
  cp /etc/iptables/iptables.rules /root/tor-backups/iptables.rules
  cp /etc/iptables/ip6tables.rules /root/tor-backups/ip6tables.rules
  cp /etc/tor/torrc /root/tor-backups/torrc

  # Configure SocksPort, DNSPort and TransPort.
  echo "SocksPort 9050
DNSPort 5353
TransPort 9040" | tee -a /etc/tor/torrc >/dev/null

  # Configure iptables rules.
  echo '
*nat
:PREROUTING ACCEPT [6:2126]
:INPUT ACCEPT [0:0]
:OUTPUT ACCEPT [17:6239]
:POSTROUTING ACCEPT [6:408]

-A PREROUTING ! -i lo -p udp -m udp --dport 53 -j REDIRECT --to-ports 5353
-A PREROUTING ! -i lo -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -j REDIRECT --to-ports 9040
-A OUTPUT -o lo -j RETURN
--ipv4 -A OUTPUT -d 192.168.0.0/16 -j RETURN
-A OUTPUT -m owner --uid-owner "tor" -j RETURN
-A OUTPUT -p udp -m udp --dport 53 -j REDIRECT --to-ports 5353
-A OUTPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -j REDIRECT --to-ports 9040
COMMIT

*filter
:INPUT DROP [0:0]
:FORWARD DROP [0:0]
:OUTPUT DROP [0:0]

-A INPUT -i lo -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
--ipv4 -A INPUT -p tcp -j REJECT --reject-with tcp-reset
--ipv4 -A INPUT -p udp -j REJECT --reject-with icmp-port-unreachable
--ipv4 -A INPUT -j REJECT --reject-with icmp-proto-unreachable
--ipv6 -A INPUT -j REJECT
--ipv4 -A OUTPUT -d 127.0.0.0/8 -j ACCEPT
--ipv4 -A OUTPUT -d 192.168.0.0/16 -j ACCEPT
--ipv6 -A OUTPUT -d ::1/8 -j ACCEPT
-A OUTPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
-A OUTPUT -m owner --uid-owner "tor" -j ACCEPT
--ipv4 -A OUTPUT -j REJECT --reject-with icmp-port-unreachable
--ipv6 -A OUTPUT -j REJECT
COMMIT' | tee /etc/iptables/iptables.rules >/dev/null

  # Apply iptables rules to IPv6 too.
  rm /etc/iptables/ip6tables.rules
  ln -s /etc/iptables/iptables.rules /etc/iptables/ip6tables.rules

  # Re-enable systemd services.
  systemctl enable --now tor.service
  systemctl enable --now iptables.service
  systemctl enable --now ip6tables.service

  printf "\\n${green}%s${endc}\\n" \
         "The transparent proxy has started. All connections will now be made over Tor."

  # Checks your IP.
  ip_check
}

stop() {
  # Disable systemd services.
  if systemctl is-active tor.service >/dev/null 2>&1; then
    systemctl stop tor.service
  fi
  if systemctl is-active iptables.service >/dev/null 2>&1; then
    systemctl stop iptables.service
  fi
  if systemctl is-active ip6tables.service >/dev/null 2>&1; then
    systemctl stop ip6tables.service
  fi

  # Delete configuration files.
  rm /etc/tor/torrc
  rm /etc/iptables/ip6tables.rules
  rm /etc/iptables/iptables.rules

  # Restore them with the backups.
  mv /root/tor-backups/torrc /etc/tor/torrc
  mv /root/tor-backups/iptables.rules /etc/iptables/iptables.rules
  mv /root/tor-backups/ip6tables.rules /etc/iptables/ip6tables.rules

  # Delete backups directory.
  rm -rf /root/tor-backups

  # Re-enable systemd services.
  systemctl start tor.service
  systemctl start iptables.service
  systemctl start ip6tables.service

  printf "\\n${red}%s${endc}\\n" \
         "The transparent proxy has ended. All connections will now be made over the clearnet."

  # Checks your IP address.
  ip_check
}

status() {
  # Check if the Tor service is active.
  if systemctl is-active tor.service >/dev/null 2>&1; then
    printf "\\n${green}%s${endc}\\n" \
           "The Tor service is running."
  else
    printf "${red}%s${endc}\\n" "The Tor service is not running."
  fi
}

# Only root can run this script.
check_root

while test $# -gt 0; do
        case "$1" in
                --start)
                       start
                       break
                       ;;
                --stop)
                       stop
                       break
                       ;;
                --status)
                       status
                       break
                       ;;
               --ip)
                       ip_check
                       break
                       ;;
               --torstatus)
                       # Checks if you're connected to Tor.
                       curl -s https://check.torproject.org/ | cat | grep -m 1 Tor
                       exit 1
                       ;;
                *)
                       echo "Transparent Proxy Through Tor."
                       echo " "
                       echo "Usage: ./transparentproxy.sh [options]"
                       echo " "
                       echo "Options:"
                       echo " "
                       echo "--start         Start transparent proxy through Tor."
                       echo "--stop          Stop transparent proxy and reset iptables."
                       echo "--status        Check status of Tor service."
                       echo "--ip            Check your public IP."
                       echo "--torstatus     Check if connected to Tor via https://check.torproject.org"
                       exit 1
                       ;;
        esac
done
