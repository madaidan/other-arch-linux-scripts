# Install and verify Tor Browser:
read -r -p "Do you want to install the Tor Browser? (y/n) " torbrowser
if [ "${torbrowser}" = "y" ] ; then
  echo "Check that the PGP signatures match."
  sleep 2
  wget https://www.torproject.org/dist/torbrowser/8.0.4/tor-browser-linux64-8.0.4_en-US.tar.xz
  wget https://dist.torproject.org/torbrowser/8.0.4/tor-browser-linux64-8.0.4_en-US.tar.xz.asc
  gpg --keyserver pool.sks-keyservers.net --recv-keys 0x4E2C6E8793298290
  gpg --fingerprint 0x4E2C6E8793298290
  gpg --verify tor-browser-linux64-8.0.4_en-US.tar.xz.asc
  sleep 5
  wget -q https://dist.torproject.org/torbrowser/8.0.4/sha256sums-unsigned-build.txt
  wget -q https://dist.torproject.org/torbrowser/8.0.4/sha256sums-unsigned-build.txt.asc
  gpg --keyserver keys.mozilla.org --recv-keys 0x4E2C6E8793298290
  gpg --verify sha256sums-unsigned-build.txt.asc sha256sums-unsigned-build.txt
  sleep 2
  sha256sum tor-browser-linux64-8.0.4_en-US.tar.xz
  echo "Compare the sha256sum with the one in 'sha256sums-unsigned-build.txt'"
  cat sha256sums-unsigned-build.txt | grep tor-browser-linux64-8.0.4_en-US.tar.xz
  
  read -r -p "Extract the Tor browser now? (y/n) "
  tar -xvJf tor-browser-linux64-8.0.4_en-US.tar.xz
fi
