#!/bin/bash -e

# Checks if you are UID 0, if not, exits.
if [[ "$(id -u)" -ne 0 ]]; then
  echo "This script needs to be run as root. Exiting."
  exit 1
fi

# Checks if GRUB is installed, if not, exits.
if ! pacman -Qq grub; then
  echo "This script can only be used with GRUB. Exiting."
  exit 1
fi

# Install AppArmor.
read -r -p "Do you want to install apparmor? (y/n) " apparmor
if [ "${apparmor}" = "y" ]; then
  # Installs AppArmor.
  pacman -S --noconfirm -q apparmor >/dev/null

  # Enable AppArmor kernel parameter.
  echo 'GRUB_CMDLINE_LINUX="$GRUB_CMDLINE_LINUX apparmor=1 security=apparmor"' | tee /etc/grub.d/30_enable_apparmor.cfg

  # Regenerate GRUB configuration file.
  grub-mkconfig -o /boot/grub/grub.cfg

  # Enable AppArmor systemd service.
  systemctl enable apparmor
fi
