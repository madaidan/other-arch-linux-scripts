# Other Arch Linux Scripts

These are other scripts than install scripts for Arch Linux. They can work with any distro if you tweak it a small bit.

## Scripts

There is a script to create a [transparent proxy through Tor](https://trac.torproject.org/projects/tor/wiki/doc/TransparentProxy), a script to automatically install and verify the Tor Browser and a script to automatically enable apparmor.

## Transparent Proxy

The transparent proxy script has a few options.

`--start` enables the transparent proxy.

`--stop` disables the transparent proxy.

`--status` checks if the Tor service is running.

`--ip` shows you your public IP address.

`--torstatus` checks if you are connected to Tor by checking https://check.torproject.org

WARNING: If using the Tor Browser with a transparent proxy then you should [disable Tor in the Tor Browser](https://theprivacyguide1.github.io/linux_hardening_guide#tor_browser_tor_over_tor) to prevent [Tor over Tor](https://www.whonix.org/wiki/DoNot#Allow_Tor_over_Tor_Scenarios).

## AppArmor script

The AppArmor script can only be used with GRUB as your bootloader. Anything else will fail.